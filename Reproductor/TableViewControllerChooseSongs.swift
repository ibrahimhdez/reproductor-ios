//
//  TableViewControllerChooseSongs.swift
//  Reproductor
//
//  Created by Ibrahim Hernández Jorge on 01/01/2019.
//  Copyright © 2019 Ibrahim Hernández Jorge. All rights reserved.
//

import UIKit

class TableViewControllerChooseSongs: UITableViewController {
    private var songs: [URL]!
    private let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    var selectedCells = [UITableViewCell]()
    var existingSongs: [URL]!
    
    override func viewDidLoad() {
        songs = getDocumentsSongs()
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        borrarCancionesExistentes()
    }
    
    private func borrarCancionesExistentes() {
        for existingSong in existingSongs {
            if let index = songs.index(of:existingSong) {
                songs.remove(at: index)
            }
        }
    }
  
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return songs.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSong", for: indexPath)
        let songTitle = self.songs[indexPath.row].lastPathComponent.prefix(self.songs[indexPath.row].lastPathComponent.count - 4).description //Quitamos la extension del nombre
        
        cell.textLabel!.text = songTitle //Asignamos a la celda el nombre de la canción
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            if(cell.accessoryType == .none) {
                cell.accessoryType = .checkmark
            }
            else {
                cell.accessoryType = .none
            }
        }
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    private func getDocumentsSongs() -> [URL] {
        let fileManager = FileManager.default
        let allfiles = try? fileManager.contentsOfDirectory(atPath: documentsPath.path)
        var songs = [URL]()
        
        for file in allfiles! {
            let index = file.index(file.endIndex, offsetBy: -3)
            let fileExtension = file.suffix(from: index)
            
            if(fileExtension == "mp3") {
                songs.append(documentsPath.appendingPathComponent(file))
            }
        }
        return songs
    }
}
