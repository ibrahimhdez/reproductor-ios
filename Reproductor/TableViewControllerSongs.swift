//
//  TableViewControllerSongs.swift
//  
//
//  Created by Ibrahim Hernández Jorge on 27/12/2018.
//

import UIKit

class TableViewControllerSongs: UITableViewController {
    private let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    var songs: [String]!
    private var choosedSong: String!
    var toolbarTitle: String!
    var songsOfList = [String : [String]]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = toolbarTitle
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return songs.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celdaCancion", for: indexPath)
        let song = self.songs[indexPath.row]
        let songTitle = song.prefix(song.count - 4).description //Quitamos extensión de la canción
        
        cell.textLabel!.text = songTitle //Asignamos a la celda el nombre de la canción
    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "playSong", sender: indexPath.row)
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if(toolbarTitle != "Todas las canciones") {
                // Delete the row from the data source
                self.songs.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                refreshUserDefaultsData()
            }
        }
    }
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let listaMovida = songs[fromIndexPath.row]
        self.songs.remove(at: fromIndexPath.row)
        self.songs.insert(listaMovida, at: to.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let index = sender as? Int
        var urlSongs = [URL]()
        
        for song in songs! {
            let urlCompleta = documentsPath.appendingPathComponent(song)
            urlSongs.append(urlCompleta)
        }
        
        if(index != nil) {
            if (segue.destination.view != nil){
                (segue.destination as! ViewController).songs = urlSongs
                (segue.destination as! ViewController).indexSong = index
            }
        }
        else {
            if (segue.destination.view != nil) {
                (segue.destination as! TableViewControllerChooseSongs).existingSongs = urlSongs
            }
        }
    }
    
    @IBAction func getSelectedSongs(segue: UIStoryboardSegue) {
        let choosedSongsController = (segue.source as! TableViewControllerChooseSongs)
    
        for i in 0...choosedSongsController.tableView.numberOfRows(inSection: 0) {
            if let cell = choosedSongsController.tableView.cellForRow(at: IndexPath(row: i, section: 0)) {
                if(cell.accessoryType == .checkmark) {
                    let titleSongSelected = cell.textLabel!.text! + ".mp3"
                    songs.append(titleSongSelected)
                }
            }
        }
        tableView.reloadData()
        refreshUserDefaultsData()
    }
    
    private func refreshUserDefaultsData() {
        songsOfList[toolbarTitle] = songs
        UserDefaults.standard.set(songsOfList, forKey: "dictionarySongsOfLists")
    }
}
