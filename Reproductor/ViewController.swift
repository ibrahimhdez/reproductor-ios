//
//  ViewController.swift
//  Reproductor
//
//  Created by Ibrahim Hernández Jorge on 26/12/2018.
//  Copyright © 2018 Ibrahim Hernández Jorge. All rights reserved.
//

import UIKit
import AVFoundation
import EFAutoScrollLabel
import MediaPlayer

/**
Clase que gestiona la ventana del reproductor
*/

class ViewController: UIViewController, AVAudioPlayerDelegate {
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var volumeSlider: UISlider!
    private var titleLabel: EFAutoScrollLabel!
    private var player = AVAudioPlayer()
    private var playing = false
    var songs: [URL]!
    var indexSong: Int!
    @IBOutlet weak var startSongTimeLabel: UILabel!
    @IBOutlet weak var endSongTimeLabel: UILabel!
    var timer = Timer()
    var songTimer = SongTimer()
    @IBOutlet weak var timeSlider: DesignableSlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(systemVolumeDidChange), name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
        
        startSongTimeLabel.text = 0.description
        timeSlider.addTarget(self, action: #selector(onTimeSliderChanged(slider:event:)), for: .valueChanged)
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        initTitleLabel()
        initMP3Info()
        initCurrentVolume()
        initPlayer()
        playSong()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(playing) {
            player.stop()
        }
    }
    
    /**
    Método para inicializar la etiqueta del título de la canción 
    */
    func initTitleLabel() {
        titleLabel = EFAutoScrollLabel(frame: CGRect(x: authorLabel.frame.minX, y: authorLabel.frame.minY - authorLabel.bounds.height * 2 - 30, width: authorLabel.bounds.width, height: authorLabel.bounds.height + 30))
        let titleSong = getCurrentSong().lastPathComponent.description
        
        titleLabel.text = titleSong.prefix(titleSong.count - 4).description //quitamos la extensión al título
        titleLabel.font = UIFont.systemFont(ofSize: 30)
        titleLabel.labelSpacing = 50                       // Distance between start and end labels
        titleLabel.pauseInterval = 1.7                     // Seconds of pause before scrolling starts again
        titleLabel.scrollSpeed = 30                        // Pixels per second
        titleLabel.textAlignment = NSTextAlignment.center    // Centers text when no auto-scrolling is applied
        titleLabel.fadeLength = 12                         // Length of the left and right edge fade, 0 to disable
        titleLabel.scrollDirection = EFAutoScrollDirection.Left
        titleLabel.observeApplicationNotifications()
        self.view.addSubview(titleLabel)
    }
    
    /**
    Método que reproduce la canción si está en pause y viceversa
    */
    @IBAction func playOrPauseSong(_ sender: Any) {
        if(playing) {
            pauseSong()
        }
        else {
            playSong()
        }
    }
    
    /**
    Listener del botón de siguiente canción
    */
    @IBAction func nextSong(_ sender: Any) {
        playNextSong()
    }
    
    /**
    Método que salta a la siguiente canción
    */
    private func playNextSong() {
        timer.invalidate() //Para el timer de la canción que estaba sonando previamente
        indexSong += 1 //Aumenta el índice para reproducir la siguiente canción de la lista
        
        //Comprueba que el índice de la siguiente canción pertenece al rango entre 0 y el número de canciones de la lista
        //Si no es correcto es porque no hay más canciones y salta a la primera (índice = 0)
        if(!isCorrectIndex()) {
            indexSong = 0
        }
        prepareForPlay(isWantPlay: playing) //Prepara todos los datos para reproducir la canción
    }
    
    /**
    Listener del botón para hacer sonar la canción anterior
    */
    @IBAction func prevSong(_ sender: Any) {
        timer.invalidate()
        indexSong -= 1
        
        //Si el índice no es correcto quiere decir que está en la primera canción, por lo que salta a la última de la lista
        if(!isCorrectIndex()) {
            indexSong = songs.count - 1
        }
        prepareForPlay(isWantPlay: playing) //Prepara todos los datos para reproducir la canción
    }
    
    /**
    Método que inicializa el reproductor y carga la información del fichero MP3
    :param: isWantPlay Bool para indicar si se quiere reproducir o no la canción una vez preparado el reproductor
    */
    private func prepareForPlay(isWantPlay: Bool) {
        initPlayer() //Preparar el reproductor
        initMP3Info() //Carga toda la información del archivo MP3
        refreshTimeLabels()
        
        if(isWantPlay) {
            playSong()
        }
    }
    
    /**
    Método para empezar o renaudar la reprouducción de una canción
    */
    private func playSong() {
        initTimer() //Inicializa el timer que llevará el progeso de la canción en segundos
        playButton.setImage(UIImage(named: "pause.PNG"), for: .normal) //El botón de reproducir/pausar aparece con la imagen de pausar
        player.play()
        playing = true //Avisa de que el reproductor está reproduciendo
    }
    
    /**
    Método para pausar la canción que actualmente se reproduce
    */
    private func pauseSong() {
        player.pause()
        timer.invalidate() //Para el timer, así no siguen aumentando los segundos mientras la canción está parada
        playButton.setImage(UIImage(named: "play.PNG"), for: .normal) //El botón de reproducir/pausar aparece con la imagen de play
        playing = false //Avisa de que ya no se está reproduciendo una canción
    }
    
    /**
     Método para obtener el volumen actual del sistema
    */
    private func initCurrentVolume() {
        volumeSlider.value = AVAudioSession.sharedInstance().outputVolume
        MPVolumeView.setVolume(volumeSlider.value)
    }
    
    /**
    Listener del slider del volumen
    */
    @IBAction func changeVolume(_ sender: Any) {
        MPVolumeView.setVolume((sender as! UISlider).value) //Cambia el volumen del sistema según el valor seleccionado con el slider)
    }
    
    /**
    Método que cuando cambie el volumen del sistema actualizará el valor del slider del volumen
    */
    @objc func systemVolumeDidChange(notification: NSNotification) {
        volumeSlider.value = (notification.userInfo?["AVSystemController_AudioVolumeNotificationParameter"] as! Float)
    }
    
    /**
    Método que extrae toda la información necesaria de un fichero MP3
    */
    private func initMP3Info() {
        let playerItem = AVPlayerItem(url: getCurrentSong()) //Carga en el reproductor la canción a reproducir
        let metadataList = playerItem.asset.metadata //Obtiene los metadatos del fichero MP3
        //Variables booleanas utilizadas para comprobar si algunos de los datos no se encuentran en el fichero MP3
        var hasAuthor = false
        var hasTitle = false
        var hasArtwork = false
       
        //Recorremos la lista de metadatos
        for item in metadataList {
            //Obtenemos la clave del metadato
            guard let key = item.commonKey?.rawValue, let value = item.value else {
                continue
            }
            //Comprobamos de que metadato se trata
            switch key {
                case "title" : do { //En caso de que se trate del título de la canción
                    titleLabel.text = value.description
                    hasTitle = true
                }
                case "artist": do { //En caso de que se trate del autor de la canción
                    self.authorLabel.text = value.description
                    hasAuthor = true
                }
            case "artwork" where value is Data: do { //En caso de que se trate de la carátula de la canción
                    coverImage.image = UIImage(data: value as! Data) //Cargamos la imagen en la interfaz de la app
                    hasArtwork = true
                }
                default:
                    continue
            }
        }
        
        //Comprobamos que datos no se han obtenido del archivo y añadimos valores por defecto
        if(!hasAuthor) {
            authorLabel.isHidden = true
        }
        else {
            authorLabel.isHidden = false
        }
        
        if(!hasTitle) {
            titleLabel.text = getCurrentSong().lastPathComponent.prefix(getCurrentSong().description.count - 4).description
        }
        
        if(!hasArtwork) {
            coverImage.image = UIImage(named: "defaultArtwork.png")
        }
    }
    
    /**
    Método para inicializar y hacer sonar la canción seleccionada de la lista
    */
    private func initPlayer() {
        player = try! AVAudioPlayer(contentsOf: getCurrentSong()) //Cargamos canción a sonar
        songTimer.initTimes(durationOfSong: NSInteger(player.duration)) //Iniciamos el timer de los segundos de la canción con la duración de la canción
        player.delegate = self //Marcamos esta clase como la delegada del reproductor. Así podemos capturar el evento de finalización de una canción
        player.prepareToPlay() //Preparamos el reproductor
        timeSlider.value = 0 //Ponemos el slider del tiempo de la canción al principio
    }
    
    /**
    Método para inicializar el timer de tiempo de la canción
    */
    private func initTimer() {
        songTimer.startTimer = songTimer.getTimer(seconds: NSInteger(player.currentTime)) //Damos al tiempo de inicio el valor actual de la canción
        songTimer.reloadEndTimer() //Damos al tiempo restante los segundos y minutos que faltan de reproducción de canción
        refreshTimeLabels() //Refrescamos las etiquetas de la interfaz de la app con los valores actuales
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.calculateAndRefreshTimeLabels), userInfo: nil, repeats: true) //Inicializamos el timer para cada segundo actualice los valores de las etiquetas de tiempo de la interfaz
    }
    
    /**
    Método que se ejecutará cuando la canción que está sonando se acabe
    */
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        sleep(1) //Esperamos un segundo para que el cambio de canción no sea brusco
        playNextSong() //Hacemos sonar la siguiente canción de la lista
    }
    
    /**
    Método que ejecuta el timer cada segundo para actualizar las etiquetas de tiempo
    */
    @objc func calculateAndRefreshTimeLabels(){
        self.songTimer.nextTime() //Actualizamos los valores de tiempo actual y tiempo restante
        timeSlider.value = songTimer.getPercentageOfSong() //Movemos el slider del tiempo al porcentaje actual de la canción
        refreshTimeLabels() //Refrescamos los labels de la interfaz
    }
    
    /**
    Método para refrescar los valores de las etiquetas de tiempo de la interfaz
    */
    private func refreshTimeLabels() {
        self.startSongTimeLabel.text = self.songTimer.startTimer //Actualizamos la etiqueta que muestra el tiempo actual de la canción
        self.endSongTimeLabel.text = self.songTimer.endTimer //Actualizamos la etiqueta que muestra el tiempo restante de la canción
    }
    
    /**
    Método que devuelve la canción actual (la seleccionada previamente de la lista)
    */
    private func getCurrentSong() -> URL {
        return songs[indexSong]
    }
    
    /**
    Método que comprueba si el índice de la canción a sonar es correcto o no. Para volver a empezar o ir al final de la lista
    :returns Devuelve true si el índice es correcto y false si no lo es
    */
    private func isCorrectIndex() -> Bool {
        return (0 <= indexSong) && (indexSong < songs.count)
    }

    /**
    Método que controla los eventos de cambios del slider de tiempo de la canción
    :param: slider Slider que recibe el evento
    :event: Tipo de evento que genera
    */
    @objc func onTimeSliderChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .moved: do {
                let minutoSeleccionado = songTimer.percentageToSeconds(percentage: self.timeSlider.value)
                
                self.songTimer.startTimer = self.songTimer.getTimer(seconds: NSInteger(minutoSeleccionado))
                //TODO Cálculo de etiqueta endTimer
                calculateAndRefreshTimeLabels()
            }
            
            case .ended: do {
                let minutoSeleccionado = songTimer.percentageToSeconds(percentage: self.timeSlider.value)
                
                self.player.currentTime = minutoSeleccionado
            }

            default:
                break
            }
        }
    }
}
