//
//  TableViewControllerLists.swift
//  Reproductor
//
//  Created by Ibrahim Hernández Jorge on 27/12/2018.
//  Copyright © 2018 Ibrahim Hernández Jorge. All rights reserved.
//

import UIKit

class TableViewControllerLists: UITableViewController, FileManagerDelegate {
    private var listasReproduccion = ["Todas las canciones"]
    private let LIST_OF_ALL_SONGS = "Todas las canciones"
    private var songsOfList = [String : [String]]()
    private let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

    override func viewDidLoad() {
        super.viewDidLoad()
        copyFilesFromBundleToDocuments()
        
        print("RUTA DEL DIRECTORIO DOCUMENTOS: \(documentsPath.path)")
        
        if(UserDefaults.standard.object(forKey: "listasReproduccion") != nil) {
            listasReproduccion = UserDefaults.standard.object(forKey: "listasReproduccion") as! [String]
        }
        
        if(UserDefaults.standard.object(forKey: "dictionarySongsOfLists") != nil) {
            songsOfList = UserDefaults.standard.object(forKey: "dictionarySongsOfLists") as! [String : [String]]
        }
        else {
            let listaReproduccionTodasLasCanciones = listasReproduccion[0]
            songsOfList[listaReproduccionTodasLasCanciones] = getDocumentsSongs()
        }
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem
    }

    private func copyFilesFromBundleToDocuments() {
        let fileManager = FileManager.default
        let pathFromBundle = Bundle.main.resourceURL!
        let pathDocuments = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
    
        do {
            let filelist = try fileManager.contentsOfDirectory(atPath: pathFromBundle.path)
            for file in filelist {
                let index = file.index(file.endIndex, offsetBy: -3)
                let fileExtension = file.suffix(from: index)
            
                if(fileExtension == "mp3") {
                    try? fileManager.copyItem(at: pathFromBundle.appendingPathComponent(file), to: pathDocuments.appendingPathComponent(file))
                }
            }
        } catch {
        
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(UserDefaults.standard.object(forKey: "dictionarySongsOfLists") != nil) {
            songsOfList = UserDefaults.standard.object(forKey: "dictionarySongsOfLists") as! [String : [String]]
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "selectedList", sender: indexPath.row)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listasReproduccion.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celdaListas", for: indexPath)
        
        cell.textLabel!.text = self.listasReproduccion[indexPath.row]
        
        return cell
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let listaABorrar = self.listasReproduccion[indexPath.row]
            
            if(listaABorrar != LIST_OF_ALL_SONGS) {
                self.listasReproduccion.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                refreshUserDefaultsData()
            }
        }
    }
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let listaMovida = listasReproduccion[fromIndexPath.row]
        self.listasReproduccion.remove(at: fromIndexPath.row)
        self.listasReproduccion.insert(listaMovida, at: to.row)
        refreshUserDefaultsData()
    }
    
    @IBAction func addLista(_ sender: Any) {
        let alert = UIAlertController(title: "Crear lista de reproducción:", message: "", preferredStyle: .alert)
        
        alert.addTextField { textField -> Void in
            textField.placeholder = "Lista de reproducción"
        }
        
        let aceptar = UIAlertAction(title: "Crear", style: .default, handler: {action in
            self.songsOfList[alert.textFields![0].text!] = [String]()
            self.listasReproduccion.append(alert.textFields![0].text!)
            self.tableView.reloadData()
            self.refreshUserDefaultsData()
        })
        
        alert.addAction(aceptar)
        present(alert, animated: true)
    }
    
    private func refreshUserDefaultsData() {
        UserDefaults.standard.set(listasReproduccion, forKey: "listasReproduccion")
        UserDefaults.standard.set(songsOfList, forKey: "dictionarySongsOfLists")
    }

    private func getDocumentsSongs() -> [String] {
        let fileManager = FileManager.default
        let allfiles = try? fileManager.contentsOfDirectory(atPath: documentsPath.path)
        var songs = [String]()
        
        for file in allfiles! {
            let index = file.index(file.endIndex, offsetBy: -3)
            let fileExtension = file.suffix(from: index)
            
            if(fileExtension == "mp3") {
                songs.append(file)
            }
        }
        return songs
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let index = sender as! Int
        
        if segue.destination.view != nil {
            let listSelected = listasReproduccion[index]
        
            (segue.destination as! TableViewControllerSongs).songs = songsOfList[listSelected]
            (segue.destination as! TableViewControllerSongs).toolbarTitle = listSelected
            (segue.destination as! TableViewControllerSongs).songsOfList = songsOfList
        }
    }
}
