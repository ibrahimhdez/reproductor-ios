//
//  DesignableSlider.swift
//  
//
//  Created by Ibrahim Hernández Jorge on 02/01/2019.
//

import UIKit

//Clase para personalizar el slider que muestra los minutos y segundos transcurridos y por sonar de una canción
@IBDesignable
class DesignableSlider: UISlider {
    @IBInspectable var thumbImage: UIImage? {
        didSet {
            setThumbImage(thumbImage, for: .normal)
        }
    }
}
