//
//  SongTimer.swift
//  Reproductor
//
//  Created by Ibrahim Hernández Jorge on 02/01/2019.
//  Copyright © 2019 Ibrahim Hernández Jorge. All rights reserved.
//

import Foundation

/**
 Clase que gestiona el tiempo que es mostrada en la interfaz del reproductor. Donde se mostrará el tiempo en minutos y segundos transcurridos de la canción y de los minutos y segundos restantes.
 */
class SongTimer {
    var startTimer: String! //Tiempo transcurrido de la canción en reproducción en formato "00:00"
    var endTimer: String! //Tiempo restante de la canción en reproducción en formato "00:00"
    var songDuration: NSInteger! //Duración total en segundos de la canción
    
    /**
    Método que inicializa los valores
    :param: durationOfSong Número de segundos que dura la canción
    */
    func initTimes(durationOfSong: NSInteger) {
        songDuration = durationOfSong //Guardamos valor de la duración de la canción
        restartStartTimer() //Ponemos a 00:00 el valor de tiempo transcurrido
        endTimer = getTimer(seconds: durationOfSong) //Ponemos en el tiempo restante la duración total de la canción
    }
    
    /**
    Método que devuelve un valor en segundos en el formato correcto de minutos y segundos ("00:00")
    :param: seconds seconds Segundos totales que van a ser formateados
    :returns: String que contiene los segundos formateados
    */
    func getTimer(seconds: NSInteger) -> String {
        let minutes = seconds / 60
        let seconds = seconds % 60
        
        return getTimeFormat(minutes: minutes, seconds: seconds)
    }
    
    /**
    Método que recarga el valor del atributo del tiempo por transcurrir
    */
    func reloadEndTimer() {
        let startTimerSeconds = toSeconds(startTimer) //Guardamos los segundos transcurridos de la canción
        let actualEndTimerSeconds = songDuration - startTimerSeconds //Restamos los segundos transcurridos a la duración de la canción para así saber cuánto queda
        
        endTimer = getTimer(seconds: actualEndTimerSeconds) //Asignamos el resultado
    }
    
    /**
    Método que pasa del formato ("00:00") a segundos
    :param: stringTimer String que contiene un tiempo formateado
    :returns: Valor en segundos del tiempo formateado
    */
    private func toSeconds(_ stringTimer: String) -> NSInteger{
        let minutes = Int(stringTimer.prefix(2))! //Obtenemos los dos primeros números que pertecen a los minutos
        let seconds = Int(stringTimer.suffix(2))! //Obtenemos los dos siguientes números que pertenecen a los segundos
        
        return minutes * 60 + seconds //Retornamos los segundos totales
    }
    
    /**
    Reestablece el valor por defecto de la primera etiqueta
    */
    private func restartStartTimer() {
        startTimer = "00:00"
    }
    
    /**
    Método que calcula los siguientes valores de las etiquetas
    */
    func nextTime() {
        startTimer = aument(time: startTimer) //Aumenta el valor de la etiqueta de tiempo transcurrido
        endTimer = decrement(time: endTimer) //Decrementa el valor de la etiqueta de tiempo restante
    }
    
    /**
    Método que aumenta el valor de una etiqueta
    :param: time tiempo a aumentar
    :returns: Tiempo resultante formateado
    */
    private func aument(time: String) -> String {
        var minutes = Int(time.prefix(2))!
        var seconds = Int(time.suffix(2))!
        
        //Si el valor de los segundos es menor que 59, aumentamos en 1
        if(seconds < 59) {
            seconds += 1
        }
        //Si no es menor de 59 quiere decir que ya pasamos al siguiente minuto
        else {
            minutes += 1
            seconds = 0 //Reestablecemos el valor de los segundos ya que es un nuevo minuto
        }
        
        return getTimeFormat(minutes: minutes, seconds: seconds) //Devuelve los minutos y segundos calculados con el formato
    }
    
    /**
    Método que decrementa el valor de una etiqueta
    :param: time Tiempo a disminuir
    :returns: Resultado de la disminución formateado
    */
    private func decrement(time: String) -> String {
        var minutes = Int(time.prefix(2))!
        var seconds = Int(time.suffix(2))!
        
        //Si hay más de 0 segundos, podemos restar 1
        if(seconds > 0) {
            seconds -= 1
        }
        //Si hay 0, quiere decir que saltamos al anterior segundo
        else {
            seconds = 59 //Completamos el valor de los segundos del nuevo minuto
            minutes -= 1
        }
        
        return getTimeFormat(minutes: minutes, seconds: seconds) //Devuelve los minutos y segundos con formato
    }
    
    /**
    Método que obtiene el porcentaje reproducido de la canción
    :returns: Porcentaje actual de la canción
    */
    func getPercentageOfSong() -> Float {
        return (Float(toSeconds(startTimer) * 100) / Float(songDuration)) / 100
    }
    
    /**
    Método que pasa de porcentaje actual de la canción a segundos
    :param: percentage Porcentaje a convertir
    :returns: Número de segundos que corresponden a ese porcentaje
    */
    func percentageToSeconds(percentage: Float) -> Double {
        let seconds = percentage * Float(songDuration)
        
        return Double(seconds)
    }
    
    /**
    Método que devuelve los minutos y segundos dados en el formato correcto de ("00:00")
    :param: minutes Minutos a formatear
    :param: seconds Segundos a formatear
    :returns: String con tiempo formateado
    */
    private func getTimeFormat(minutes: Int, seconds: Int) -> String {
        return "\(String(format: "%02d", minutes)):\(String(format: "%02d", seconds))"
    }
}
